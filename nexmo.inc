<?php

/**
 * @file
 * Functions for the Nexmo module.
 */


/**
 * Process incoming Nexmo get request
 */

function nexmo_new_message() {

  /**
   * Parsing the full url. Does not work with the $get_string because
   * nexmo adds a question mark at the beginning of the query string
   */

  $url = $_SERVER ['REQUEST_URI'];

  //Convert query string to array
  parse_str($url, $variables);

  nexmo_save_in_table($variables);

  return "success";

}

/**
 * Maps Nexmo query string parts to SQL table fields
 * Required because
 * - some fields may be restricted keywords in SQL
 * - Nexmo url might contain new query string parts that are not in the db yet
 */

function nexmo_map_to_fields($variables) {

  $nexmo_fields = array(
    'concat-part' => 'concat-part',
    'concat-ref' => 'concat-ref',
    'concat-total' => 'concat-total',
    'concat' => 'concat',
    'data' => 'data',
    'keyword' => 'keyword',
    'message_timestamp' => 'message_timestamp',
    'messageId' => 'messageId',
    'msisdn' => 'msisdn',
    'sender' => 'sender',
    'text' => 'text',
    'to' => 'destination',
    'type' => 'type',
    'udh' => 'udh',

  );

  //only keep $variables that with allowed keys
  $variables = array_intersect_key($variables, $nexmo_fields);

  /**
   * Prepare the sql table field names and values for db_insert
   */

  $fields = array();

  foreach ($variables as $key => $value) {

    $fields [$nexmo_fields[$key]] = $value;

  };

  $fields ['created'] = REQUEST_TIME;

  if (!empty ($fields ['message_timestamp'])) {
    $fields['sent'] = strtotime($fields ['message-timestamp']);
  }

  return ($fields);

}

/**
 * @param $variables
 *    the nexmo variables
 * @throws \Exception
 */
function nexmo_save_in_table($variables) {

  $fields = nexmo_map_to_fields($variables);

  if (!empty($fields['text'])) {
    /**
     * Insert new row for received message
     */
    db_insert('nexmo_inbound')
      ->fields($fields)
      ->execute();
  }
}